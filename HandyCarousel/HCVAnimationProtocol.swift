//
//  HandyCarouselViewAnimationProtocol.swift
//  HandyCarousel
//
//  Created by Andrey Zamogilin on 11.11.14.
//  Copyright (c) 2014 Andrey Zamogilin. All rights reserved.
//

import UIKit

public protocol HCVAnimationProtocol
{
    /// Prepares the views for animations.
    ///
    /// Called before the animation.
    /// In this method, you can make some manipulations on views,
    /// before doing the animation.
    /// For example, you can add a subviews.
    ///
    /// @param HandyCarouselView carouselView
    /// @param UIView current Сurrent view.
    /// @param UIView next The view to be displayed.
    func carouselViewAnimationPrepare(carouselView: HandyCarouselView, current: UIView, next: UIView);
    
    /// Performs animation.
    ///
    /// In this method, you must implement to animation.
    /// @see UIView.animateWithDuration
    ///
    /// @param HandyCarouselView carouselView
    /// @param UIView current Сurrent view.
    /// @param UIView next The view to be displayed.
    /// @param (Bool)->Void completion This method should be called in your implementation.
    func carouselViewPerformAnimation(carouselView: HandyCarouselView, current: UIView,
        next: UIView, speed: NSTimeInterval, completion: (Bool)->Void);
}
