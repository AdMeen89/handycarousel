//
//  HandyCarouselViewProtocol.swift
//  HandyCarousel
//
//  Created by Andrey Zamogilin on 11.11.14.
//  Copyright (c) 2014 Andrey Zamogilin. All rights reserved.
//

import UIKit

protocol HandyCarouselViewProtocol
{
    /// The method is called when the item tap on the carousel.
    ///
    /// @param HandyCarouselView carouselView
    /// @param Int index
    func carouselView(carouselView: HandyCarouselView, selectRowAtIndex index: Int);
}
