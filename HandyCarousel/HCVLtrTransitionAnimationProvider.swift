//
//  HCVLtrTransitionAnimationProvider.swift
//  HandyCarousel
//
//  Created by Andrey Zamogilin on 11.11.14.
//  Copyright (c) 2014 Andrey Zamogilin. All rights reserved.
//

import UIKit

/// Easy transition from left to right.
public class HCVTransitionLtrAnimationProvider: HCVAnimationProtocol
{
    /// Prepares the views for animations.
    public func carouselViewAnimationPrepare(carouselView: HandyCarouselView, current: UIView, next: UIView)
    {
        carouselView.addSubview(next)
        next.frame.origin.x = carouselView.frame.size.width
    }
    
    /// Performs animation.
    public func carouselViewPerformAnimation(carouselView: HandyCarouselView, current: UIView, next: UIView,
        speed: NSTimeInterval, completion: (Bool)->Void)
    {
        UIView.animateWithDuration( speed,
            animations:
            {
                current.frame.origin.x = -carouselView.frame.width
                next.frame.origin.x = 0
            },
            completion:
            {
                (success) -> Void in
                if success == true {
                    current.removeFromSuperview()
                }
                completion(success)
        })
    }
}
