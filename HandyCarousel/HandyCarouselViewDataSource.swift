//
//  HandyCarouselViewDataSource.swift
//  HandyCarousel
//
//  Created by Andrey Zamogilin on 11.11.14.
//  Copyright (c) 2014 Andrey Zamogilin. All rights reserved.
//

import UIKit

protocol HandyCarouselViewDataSource
{
    /// The method should return the number of elements in the carousel.
    func numberOfCarouselItems(carouselView: HandyCarouselView) -> Int;
    
    /// The method must return the form to display the position @index of the carousel.
    func carouselItemViewAtIndex(carouselView: HandyCarouselView, index: Int) -> UIView
}