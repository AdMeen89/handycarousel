//
//  HandyCarouselViewSwipeDirection.swift
//  HandyCarousel
//
//  Created by Andrey Zamogilin on 11.11.14.
//  Copyright (c) 2014 Andrey Zamogilin. All rights reserved.
//

import Foundation

enum HCVSwipeDirection
{
    case Left
    case Right
    case Up
    case Down
    case LeftAndRight
    case UpAndDown
    case All
}